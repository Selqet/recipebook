package com.selqet.momrecipebook.model;

import lombok.Data;
import org.postgresql.util.PGInterval;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
public class RecipeStepDto {
    private Long id;
    private Short index;
    private String description;
    private String time;
}
