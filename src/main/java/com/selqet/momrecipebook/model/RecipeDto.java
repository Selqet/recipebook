package com.selqet.momrecipebook.model;

import lombok.Data;
import org.postgresql.util.PGInterval;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class RecipeDto {
    private Long id;
    private String title;
    private String time;
    private List<RecipeStepDto> recipeSteps;
}
