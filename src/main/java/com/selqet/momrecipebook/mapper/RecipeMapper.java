package com.selqet.momrecipebook.mapper;

import com.selqet.momrecipebook.entity.RecipeEntity;
import com.selqet.momrecipebook.model.RecipeDto;
import com.selqet.momrecipebook.model.RecipeStepDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface RecipeMapper {
    RecipeEntity toEntity(RecipeDto recipe);

    RecipeEntity toEntity(RecipeDto recipe, @MappingTarget RecipeEntity recipeEntity);

    @Mapping(source = "recipe.id", target = "id")
    @Mapping(source = "recipe.title", target = "title")
    @Mapping(source = "recipe.time", target = "time")
    @Mapping(source = "stepsList", target = "recipeSteps")
    RecipeDto toDto(RecipeEntity recipe, List<RecipeStepDto> stepsList);
}
