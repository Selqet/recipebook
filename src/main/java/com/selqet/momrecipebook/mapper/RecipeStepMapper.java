package com.selqet.momrecipebook.mapper;

import com.selqet.momrecipebook.entity.RecipeEntity;
import com.selqet.momrecipebook.entity.RecipeStepEntity;
import com.selqet.momrecipebook.model.RecipeStepDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface RecipeStepMapper {
    RecipeStepEntity toEntity(RecipeStepDto recipeStep);

    @Mapping(source = "recipe", target = "recipe")
    @Mapping(source = "step.id", target = "id")
    @Mapping(source = "step.index", target = "index")
    @Mapping(source = "step.description", target = "description")
    @Mapping(source = "step.time", target = "time")
    RecipeStepEntity toEntity(RecipeStepDto step, RecipeEntity recipe);

    RecipeStepDto toDto(RecipeStepEntity recipeStep);


}
