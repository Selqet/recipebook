package com.selqet.momrecipebook.controller;

import com.selqet.momrecipebook.entity.RecipeEntity;
import com.selqet.momrecipebook.model.RecipeDto;
import com.selqet.momrecipebook.repository.RecipeRepository;
import com.selqet.momrecipebook.service.impl.RecipeServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/recipes")
public class RecipeController {

    private RecipeServiceImpl recipeService;

    public RecipeController(RecipeServiceImpl recipeService) {
        this.recipeService = recipeService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<RecipeDto>> getAllRecipes() {
        List<RecipeDto> recipes =  recipeService.getAllRecipes();
        return new ResponseEntity<>(recipes, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Object> addRecipe(@RequestBody RecipeDto recipe) {
        recipeService.addRecipe(recipe);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteRecipe(@PathVariable Long id) {
        recipeService.deleteRecipe(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateRecipe(@PathVariable Long id, @RequestBody RecipeDto recipe) {
        recipeService.updateRecipe(id, recipe);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
