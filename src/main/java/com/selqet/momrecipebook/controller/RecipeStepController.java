package com.selqet.momrecipebook.controller;

import com.selqet.momrecipebook.model.RecipeStepDto;
import com.selqet.momrecipebook.service.impl.RecipeServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/recipes/{id}/step")
public class RecipeStepController {

    private RecipeServiceImpl recipeService;

    public RecipeStepController(RecipeServiceImpl recipeService) {
        this.recipeService = recipeService;
    }

    //Trying to overload

    @PostMapping
    public ResponseEntity<Object> addStep(@PathVariable Long id, @RequestBody RecipeStepDto step) {
        recipeService.addStep(id, step);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/{index}")
    public ResponseEntity<Object> deleteStep(@PathVariable Long id, @PathVariable Short index) {
        recipeService.deleteStep(id, index);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
