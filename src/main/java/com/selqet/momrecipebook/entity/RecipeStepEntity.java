package com.selqet.momrecipebook.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.hibernate.annotations.TypeDef;

import org.postgresql.util.PGInterval;
import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;


@Data
@Entity
@Table(schema = "public", name = "recstep")
public class RecipeStepEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recipestep_generator")
    @SequenceGenerator(name = "recipestep_generator", sequenceName = "recstep_step_id_seq", allocationSize = 1)
    @Column(name = "step_id")
    private Long id;

    @Column(name = "step_index")
    private Short index;

    @Column(name = "step_descr")
    private String description;

    @Column(name = "step_time")
    private String time;

    @ManyToOne
    @JoinColumn(name = "rec_id", nullable = false)
    @JsonBackReference
    private RecipeEntity recipe;
}
