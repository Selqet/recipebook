package com.selqet.momrecipebook.service.impl;

import com.selqet.momrecipebook.entity.RecipeEntity;
import com.selqet.momrecipebook.entity.RecipeStepEntity;
import com.selqet.momrecipebook.mapper.RecipeMapper;
import com.selqet.momrecipebook.mapper.RecipeStepMapper;
import com.selqet.momrecipebook.model.RecipeDto;
import com.selqet.momrecipebook.model.RecipeStepDto;
import com.selqet.momrecipebook.repository.RecipeRepository;
import com.selqet.momrecipebook.repository.RecipeStepRepository;
import com.selqet.momrecipebook.service.RecipeService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


//TODO Divide services!
//TODO REFACTOR
@Service
public class RecipeServiceImpl implements RecipeService {
    private final RecipeRepository recipeRepository;
    private final RecipeMapper recipeMapper;
    private final RecipeStepRepository recipeStepRepository;
    private final RecipeStepMapper recipeStepMapper;

    public RecipeServiceImpl(RecipeRepository recipeRepository, RecipeMapper recipeMapper, RecipeStepRepository recipeStepRepository, RecipeStepMapper recipeStepMapper) {
        this.recipeRepository = recipeRepository;
        this.recipeMapper = recipeMapper;
        this.recipeStepRepository = recipeStepRepository;
        this.recipeStepMapper = recipeStepMapper;
    }

    public List<RecipeDto> getAllRecipes() {
        List<RecipeEntity> recipeEntities = recipeRepository.findAll();
        List<RecipeDto> recipeDtos = new ArrayList<>();

        for (RecipeEntity recipe : recipeEntities) {
            List<RecipeStepDto> recipeStepDtos = new ArrayList<>();

            List<RecipeStepEntity> recipeSteps = recipe.getRecipeSteps();
            for (RecipeStepEntity recipeStep : recipeSteps) {
                recipeStepDtos.add(toDtoStepAutoMapping(recipeStep));
            }
            recipeDtos.add(toDtoRecipeAutoMapping(recipe, recipeStepDtos));
        }
        return recipeDtos;
    }

    public void addRecipe(RecipeDto recipe) {
        RecipeEntity recipeEntity = toEntityRecipeAutoMapping(recipe);
        recipeRepository.save(recipeEntity);
    }

    public void deleteRecipe(Long id) {
        recipeRepository.deleteById(id);
    }

    public void updateRecipe(Long id, RecipeDto recipe) {
        RecipeEntity recipeEntity = recipeRepository.getReferenceById(id);
        recipeEntity = toEntityRecipeAutoMapping(recipe, recipeEntity);
        recipeRepository.save(recipeEntity);
    }

    //TODO Refactor
    public void addStep(Long id, RecipeStepDto step) {
        RecipeEntity recipe = recipeRepository.getReferenceById(id);
        RecipeStepEntity stepEntity = toEntityStepAutoMapping(recipe, step);
        recipeStepRepository.save(stepEntity);
    }

    //TODO Train crush!
    public void deleteStep(Long id, Short index) {
        RecipeEntity recipe = recipeRepository.getReferenceById(id);

        for (RecipeStepEntity step: recipe.getRecipeSteps()) {
            if (index.equals(step.getIndex())) {
                recipeStepRepository.delete(step);
                break;
            }
        }
    }

    private RecipeDto toDtoRecipeAutoMapping(RecipeEntity recipe, List<RecipeStepDto> recipeSteps) {
        return recipeMapper.toDto(recipe, recipeSteps);
    }

    private RecipeStepDto toDtoStepAutoMapping(RecipeStepEntity recipeStep) {
        return recipeStepMapper.toDto(recipeStep);
    }

    private RecipeEntity toEntityRecipeAutoMapping(RecipeDto recipe) {
        return recipeMapper.toEntity(recipe);
    }

    private RecipeEntity toEntityRecipeAutoMapping(RecipeDto recipe, RecipeEntity recipeEntity) {
        return recipeMapper.toEntity(recipe, recipeEntity);
    }

    private RecipeStepEntity toEntityStepAutoMapping(RecipeEntity recipe, RecipeStepDto step) {
        return recipeStepMapper.toEntity(step, recipe);
    }

}
